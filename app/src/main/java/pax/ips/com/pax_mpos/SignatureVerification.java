package pax.ips.com.pax_mpos;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CheckBox;

import java.util.concurrent.TimeUnit;

import pax.ips.com.pax_mpos.App2AppData.Parameters;
import pax.ips.com.pax_mpos.App2AppData.enums.PaymentType;

public class SignatureVerification extends AppCompatActivity {


    CheckBox amountConfirmation;
    CheckBox printPedReceipt;


    private static final String PAYMENT_RESULT_HOST = "paymentresult";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signature_verification);
        amountConfirmation = (CheckBox) findViewById(R.id.amountConfirmation);
        printPedReceipt = (CheckBox) findViewById(R.id.printPedReceipt);
    }


    public void onYesButtonClick(View v) {

        finish();
    }

    public void onNoButtonClick(View v) throws InterruptedException {
        Uri.Builder builder = new Uri.Builder();
        builder.scheme("demopax")
                .authority(PAYMENT_RESULT_HOST);

        String uriResponse = builder.build().toString();
        builder = new Uri.Builder();
        builder.scheme("pax")
                .authority("payment")
                .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.REVERSAL.toString())
                .appendQueryParameter(Parameters.CALLER_TRX_ID, "test")
                .appendQueryParameter(Parameters.RECEIPT_BY_ECR, Boolean.toString(true))
                .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse);

        Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(intent);
        finish();
    }
}
