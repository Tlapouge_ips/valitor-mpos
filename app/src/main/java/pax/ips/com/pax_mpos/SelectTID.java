package pax.ips.com.pax_mpos;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class SelectTID extends MainActivity {


    protected void onCreate(Bundle savedInstanceState) {


        super.onCreate(savedInstanceState);
        setContentView(R.layout.tidselection);
        tidSelection = (EditText) findViewById(R.id.tidSelection);
        Button confirm = (Button) findViewById(R.id.confirm);
        confirm.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View arg0) {

                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority("service");
                String uriResponse = builder.build().toString();
                builder = new Uri.Builder();
                builder.scheme("pax")
                        .authority("service")
                        .appendQueryParameter("callerPackage", BuildConfig.APPLICATION_ID)
                        .appendQueryParameter("serviceOperation", "SELECT_TID")
                        .appendQueryParameter("terminalId", tidSelection.getText().toString())
                        .appendQueryParameter("uri", uriResponse);
                Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);
                finish();
            }

        });

    }

}
