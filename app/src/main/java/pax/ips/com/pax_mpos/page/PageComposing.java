package pax.ips.com.pax_mpos.page;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.AssetManager;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Build;
import android.support.annotation.RequiresApi;

import com.pax.dal.IDAL;
import com.pax.dal.IPrinter;
import com.pax.dal.exceptions.PrinterDevException;
import com.pax.gl.page.IPage;
import com.pax.gl.page.PaxGLPage;
import com.pax.neptunelite.api.NeptuneLiteUser;

import java.io.IOException;
import java.io.InputStream;

import pax.ips.com.pax_mpos.MainActivity;

public class PageComposing  {
    Context context;
    public PaxGLPage iPaxGLPage;
    private Bitmap bitmap;

    private static final int FONT_BIG = 28;
    private static final int FONT_NORMAL = 24;
    private static final int FONT_BIGEST = 40;
    byte a = 2;
    byte by = 2;

    public PageComposing(Context context) {

        this.context = context;

    }



    @TargetApi(Build.VERSION_CODES.KITKAT)
    @RequiresApi(api = Build.VERSION_CODES.KITKAT)
    public void run() {

        iPaxGLPage = PaxGLPage.getInstance(context);

        IPage page = iPaxGLPage.createPage();

        String Receipt = MainActivity.receiptView.getText().toString();
        String ExtraFields = MainActivity.result.getText().toString();

        String standardFontPath ="/data/data/pax.ips.com.pax_mpos/app_fonts/RobotoMono-Regular.ttf";


        if (Receipt != "") {



            page.setTypeFace(standardFontPath);
            System.out.println(page.getTypeFace());


            IPage.ILine.IUnit unit = page.createUnit();
            unit.setAlign(IPage.EAlign.CENTER);
            unit.setText("TRANSACTION RESULTS");
            page.addLine().addUnit("RECEIPT:", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(Receipt, FONT_NORMAL, IPage.EAlign.LEFT, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
           // page.addLine().addUnit("\n", FONT_NORMAL);
            page.addLine().addUnit("EXTRA FIELDS:", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            //page.addLine().addUnit("\n", FONT_NORMAL);
            page.addLine().addUnit(ExtraFields, FONT_NORMAL, IPage.EAlign.LEFT, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit("Valitor MPOS", FONT_NORMAL, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit("Version 1.1.1", FONT_NORMAL, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(getImageFromAssetsFile("pt.bmp"));
            page.addLine().addUnit(" ", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(" ", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
            page.addLine().addUnit(" ", FONT_BIG, IPage.EAlign.CENTER, IPage.ILine.IUnit.TEXT_STYLE_NORMAL);
        }
        int width = 384;
        Bitmap bitmap = iPaxGLPage.pageToBitmap(page, width);

        setBitmap(bitmap);
        //storeImage(bitmap);

    }



    public void printBitmap(Bitmap b) {
        boolean result = false;
        if (b == null) {
            return;
        }

        IDAL dal = null;
        try {
            dal = NeptuneLiteUser.getInstance().getDal(this.context);
        } catch (Exception e) {

        }

        final IPrinter printer = dal.getPrinter();

        try {

            printer.init();

            //printer.spaceSet(a,by);
            //printer.fontSet(EFontTypeAscii.FONT_32_48,EFontTypeExtCode.FONT_48_48);
            printer.print(b, new IPrinter.IPinterListener() {
                @Override
                public void onSucc() {
                }

                @Override
                public void onError(int i) {

                }
            });
        } catch (PrinterDevException e) {

        }

        return;

    }


    public Bitmap getImageFromAssetsFile(String fileName) {
        Bitmap image = null;
        AssetManager am = context.getResources().getAssets();
        try {
            InputStream is = am.open(fileName);
            image = BitmapFactory.decodeStream(is);
            is.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return image;

    }

    public void setBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;

    }

    public Bitmap getBitmap() {
        return this.bitmap;
    }


    public enum ErrorCodes {
        ERR_NO_MEMORY,
        SUCCESS,
        ERR_FILE_ACCESS,
        ERR_FILE_NOT_FOUND;
    }



}
