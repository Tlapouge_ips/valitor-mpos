package pax.ips.com.pax_mpos.App2AppData.enums;

public enum Result {
    DECLINED,  APPROVED,  NOT_DEFINED;

    private Result(){

    }
}
