package pax.ips.com.pax_mpos.App2AppData;

public class Parameters {


        public static final String CALLER_PACKAGE = "callerPackage";
        public static final String RESPONSE_URI = "uri";
        public static final String CALLER_TRX_ID = "callerTrxId";
        public static final String PAYMENT_TYPE = "paymentType";
        public static final String AMOUNT = "amount";
        public static final String AMOUNT_CONFIRMATION = "confirmOperation";
        public static final String PREAUTHORIZATION_CODE = "preauthCode";
        public static final String SERVICE_OPERATION = "serviceOperation";
        public static final String TERMINAL_ID = "terminalId";
        public static final String RESULT = "result";
        public static final String RESULT_MESSAGE = "message";
        public static final String RESULT_OPT_MESSAGE = "optMessage";
        public static final String EXTENDED_RESULT_DATA_URI = "extendedUri";
        public static final String ENABLE_EXTENDED_RESULTS = "enable";
        public static final String RECEIPT_BY_ECR = "receiptByEcr";
        public static final String ADDITIONAL_FIELD_47_DATA = "additionalField47data";
}


