package pax.ips.com.pax_mpos.scancodec;

import android.content.Context;

import com.pax.dal.IScanCodec;
import com.pax.dal.entity.DecodeResult;

import pax.ips.com.pax_mpos.MainActivity;
import pax.ips.com.pax_mpos.util.BaseTester;
import pax.ips.com.pax_mpos.util.GetObj;

public class ScanCodecTester extends BaseTester {

    private static ScanCodecTester codecTester;
    private IScanCodec scanCodec;

    public ScanCodecTester() {

    }

    public static synchronized ScanCodecTester getInstance() {
        if (codecTester == null) {
            codecTester = new ScanCodecTester();
        }
        codecTester.scanCodec = MainActivity.getDal().getScanCodec();
        return codecTester;
    }

    public void disableFormat(int format) {
        scanCodec.disableFormat(format);
        logTrue("disableFormat");
    }

    public void enableFormat(int format) {
        scanCodec.enableFormat(format);
        logTrue("enableFormat");
    }

    public void init(Context context, int width, int height) {
        scanCodec.init(context, width, height);
        logTrue("init");
    }

    public DecodeResult decode(byte[] data) {
        DecodeResult result = scanCodec.decode(data);
        logTrue("decode");
        return result;
    }

    public void release() {
        scanCodec.release();
        logTrue("release");
    }
}
