package pax.ips.com.pax_mpos.base;

import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.view.Window;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ListView;

import pax.ips.com.pax_mpos.MainActivity;
import pax.ips.com.pax_mpos.R;

import pax.ips.com.pax_mpos.SelectTID;
import pax.ips.com.pax_mpos.scancodec.CodecFragment;


import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import pax.ips.com.pax_mpos.util.BackListAdapter;
import pax.ips.com.pax_mpos.util.FloatView;

public class DALTestActivity extends AppCompatActivity {


    private List<String> nameList = null;
    public FloatView floatView;
    private Button scan;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        requestWindowFeature(Window.FEATURE_NO_TITLE);
        setContentView(R.layout.activity_dal_test);

      //  floatView = FloatView.getInstance(DALTestActivity.this);
        //floatView.createFloatView(20, 20);

        nameList = new ArrayList<String>();
        String[] catalogueArray = getResources().getStringArray(R.array.catalogue);
        nameList = Arrays.asList(catalogueArray);

        // ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, nameList);
        final BackListAdapter adapter = new BackListAdapter(nameList, this);
        fragmentSelect(new CodecFragment());
        // default select
        // fragmentSelect(new MagFragment());



    }

    @Override
    protected void onResume() {
       // setScreen();
        super.onResume();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
    }

    private void fragmentSelect(Fragment fragment) {
        FragmentManager fManager = getSupportFragmentManager();
        FragmentTransaction transaction = fManager.beginTransaction();
        transaction.replace(R.id.parent_layout, fragment);
        transaction.commit();
    }

    private void setScreen() {
        int width = getWindowManager().getDefaultDisplay().getWidth();
        int height = getWindowManager().getDefaultDisplay().getHeight();
        if (width > height) {
            // do nothing
        } else {
           setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_LANDSCAPE);
        }
    }
}
