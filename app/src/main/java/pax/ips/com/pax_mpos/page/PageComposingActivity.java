package pax.ips.com.pax_mpos.page;

import android.app.Activity;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.widget.Toast;

import java.util.concurrent.TimeUnit;

import pax.ips.com.pax_mpos.R;


public class PageComposingActivity extends Activity {

    public static int pstion = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.pagecomposing);
        PageComposing PageComposing = new PageComposing(PageComposingActivity.this);
        PageComposing.run();
        Bitmap receipt = PageComposing.getBitmap();
        Bitmap logo = BitmapFactory.decodeResource(getResources(), R.drawable.logo);

        try {

            if (receipt == null) {
                Toast.makeText(PageComposingActivity.this, "bitmap is null", Toast.LENGTH_SHORT).show();
                return;
            }

            PageComposing.printBitmap(logo);
            TimeUnit.SECONDS.sleep(3);
            PageComposing.printBitmap(receipt);


          /*  char[] Receipt = new char[90];
            Receipt=MainActivity.receiptView.getText();

            Intent myIntent = new Intent(this, SignatureVerification.class);
            startActivity(myIntent);*/
            finish();


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

}

