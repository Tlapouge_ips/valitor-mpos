package pax.ips.com.pax_mpos.App2AppData.enums;

public enum PaymentType {

    PAYMENT,  PURCHASE,  REVERSAL,  REFUND,  AUTHORIZATION,  PREAUTHORIZATION,  CLOSE_PREAUTHORIZATION,  INTEGRATIVE_AUTHORIZATION,  NOT_DEFINED;

    private PaymentType() {
    }

}
