package pax.ips.com.pax_mpos;

import android.app.AlertDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.os.ParcelFileDescriptor;
import android.preference.PreferenceManager;
import android.support.v7.app.AppCompatActivity;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;
import com.pax.dal.IDAL;
import com.pax.neptunelite.api.NeptuneLiteUser;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileDescriptor;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

import pax.ips.com.pax_mpos.App2AppData.Configuration;
import pax.ips.com.pax_mpos.App2AppData.Parameters;
import pax.ips.com.pax_mpos.App2AppData.enums.PaymentType;
import pax.ips.com.pax_mpos.App2AppData.enums.ServiceOperation;
import pax.ips.com.pax_mpos.base.DALTestActivity;
import pax.ips.com.pax_mpos.page.PageComposingActivity;
import pax.ips.com.pax_mpos.preauth.Preauth;
import pax.ips.com.pax_mpos.preauth.PreauthTopUp;
import pax.ips.com.pax_mpos.scanner.ScannerTester;
import pax.ips.com.pax_mpos.util.GetObj;

import static com.pax.dal.entity.EScannerType.REAR;


public class MainActivity extends AppCompatActivity {

    private static final String PAYMENT_RESULT_HOST = "paymentresult";
    private static final String SERVICE_RESULT_HOST = "serviceresult";
    public Handler handler;


    public EditText amount;
    public EditText callTrx;
    public EditText PreauthCode;
    public EditText amountPreauth;
    public EditText tidSelection;

    public CheckBox amountConfirmation;
    public CheckBox printPedReceipt;

    private Button purchaseButton;
    private Button preauthButton;
    private Button topUpButton;
    private Button refundButton;
    private Button reversalButton;
    private Button closeSessionButton;
    private Button onlineTotalsButton;
    private Button preauthCompletionButton;
    private Button selectTID;
    private Button scannerButton;
    private Button aztecScannerButton;
    private Button  scannerButton2;
    public static TextView result;
    public static TextView receiptView;
    private TextView terminalstatus;
    private static IDAL dal;
    private static Context appContext;
    Context context = this;
    SharedPreferences pref;


    public enum AppStart {
        FIRST_TIME, FIRST_TIME_VERSION, NORMAL;
    }

    /**
     * The app version code (not the version name!) that was used on the last
     * The app version code (not the version name!) that was used on the last
     * start of the app.
     */
    private static final String LAST_APP_VERSION = "last_app_version";

    /**
     * Finds out started for the first time (ever or in the current version).<br/>
     * <br/>
     * Note: This method is <b>not idempotent</b> only the first call will
     * determine the proper result. Any subsequent calls will only return
     * {@link AppStart#NORMAL} until the app is started again. So you might want
     * to consider caching the result!
     *
     * @return the type of app start
     */
    public AppStart checkAppStart() {
        PackageInfo pInfo;
        SharedPreferences sharedPreferences = PreferenceManager
                .getDefaultSharedPreferences(this);
        AppStart appStart = AppStart.NORMAL;
        try {
            pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int lastVersionCode = sharedPreferences
                    .getInt(LAST_APP_VERSION, -1);
            int currentVersionCode = pInfo.versionCode;
            appStart = checkAppStart(currentVersionCode, lastVersionCode);
            // Update version in preferences
            sharedPreferences.edit()
                    .putInt(LAST_APP_VERSION, currentVersionCode).commit();
        } catch (PackageManager.NameNotFoundException e) {

        }
        return appStart;
    }

    public AppStart checkAppStart(int currentVersionCode, int lastVersionCode) {
        if (lastVersionCode == -1) {
            return AppStart.FIRST_TIME;
        } else if (lastVersionCode < currentVersionCode) {
            return AppStart.FIRST_TIME_VERSION;
        } else if (lastVersionCode > currentVersionCode) {
            return AppStart.NORMAL;
        } else {
            return AppStart.NORMAL;
        }
    }


    void extractFont(String name) {
        ;
        // read font file contents in memory
        byte[] fontData = readAsset(name);

        if (fontData != null) {

            int nameIdx = name.lastIndexOf("/");
            String fileName = name;
            String fileDir = null;
            if (nameIdx != -1) {
                fileName = name.substring(nameIdx + 1, name.length());
                fileDir = name.substring(0, nameIdx);
            }

            writeInInternalMemory(fileName, fileDir, fontData);
        }
    }


    private int writeInInternalMemory(String name, String folder, byte[] data) {

        FileOutputStream fileOutputStream = null;
        int res = -1;

        if (name == null) {
            return -1;
        }

        File outputFile = null;

        if ((folder != null) && !TextUtils.isEmpty(folder)) {
            File outputDir = context.getDir(folder, Context.MODE_PRIVATE); //Creating an internal dir
            if (!outputDir.exists()) {
                outputDir.mkdirs();
            }
            outputFile = new File(outputDir, name); //Get a file within the dir.
        } else {
            outputFile = new File(context.getFilesDir().getAbsolutePath() + File.separator + name);
        }


        try {
            // open the file (throws FileNotFoundException)
            //fileOutputStream =  this.openFileOutput(name, Context.MODE_PRIVATE);
            fileOutputStream = new FileOutputStream(outputFile);


            if (fileOutputStream != null) {
                // write file content (throws IOException)
                fileOutputStream.write(data);

                // flush file content (throws IOException)
                fileOutputStream.flush();

                res = 0;
            } else {
                // is it possible???
                res = -1;
            }
        } catch (FileNotFoundException e) {
            Log.e("FM", e.getMessage());
            res = -2;
        } catch (IOException e) {
            Log.e("FM", e.getMessage());
            res = -3;
        } finally {
            if (fileOutputStream != null) {
                try {
                    fileOutputStream.close();
                } catch (IOException e) {
                    Log.e("FM", e.getMessage());
                }
            }
        }

        return res;
    }


    byte[] readAsset(String name) {
        byte[] data = null;
        byte[] crc32data = {0, 0, 0, 0};
        long dataLen = 0;
        // String lastError = null;
        //  lastError = ErrorCodes.SUCCESS;

        if (name == null)
            return null;

        try (InputStream is = this.context.getAssets().open(name)) {
            if (is != null) {
                dataLen = is.available();
                boolean checkCrc = true;
                if (checkCrc) {
                    data = new byte[(int) (dataLen - 4)];
                } else {
                    data = new byte[(int) dataLen];
                }

                if (data == null) {
                    // lastError = ErrorCodes.ERR_NO_MEMORY;
                } else {
                    // read file contents
                    is.read(data);
                    if (checkCrc) {
                        // read CRC32
                        is.read(crc32data);
                    }

                    //   lastError = ErrorCodes.SUCCESS;
                }
            } else {
                // is it possible???
                //    lastError = ErrorCodes.ERR_FILE_ACCESS;
            }
        } catch (IOException e) {
            //     lastError = ErrorCodes.ERR_FILE_NOT_FOUND;
            //    Log.e(LOG_TAG, "File " + name + " not found:" + e.getMessage());
        }

        return data;
    }

    public StringBuilder stringConversionToTagDF8105(char[] ch) {

        StringBuilder hex = new StringBuilder();

        for (char c : ch) {
            int i = (int) c;
            // Step-3 Convert integer value to hex using toHexString() method.
            hex.append(Integer.toHexString(i).toUpperCase());
        }

        int length = hex.length() / 2;
        String hexLgth = String.format("%1$02X", length);

        StringBuilder hexLength = new StringBuilder();

        if (length > 0) {
            hexLength.append("DF8105");
        }

        if (length > 0) {
            hexLength.append(hexLgth);

            hexLength.append(hex);
        }

        return hexLength;
    }

    public void displayScanResult(String scanResult){
        Toast.makeText(this, "BARCODE CONTENT: " + scanResult, Toast.LENGTH_LONG).show();

    }

    public static IDAL getDal(){
        if(dal == null){
            try {
                long start = System.currentTimeMillis();
                dal = NeptuneLiteUser.getInstance().getDal(appContext);
                Log.i("Test","get dal cost:"+(System.currentTimeMillis() - start)+" ms");
            } catch (Exception e) {
                e.printStackTrace();
                Toast.makeText(appContext, "error occurred,DAL is null.", Toast.LENGTH_LONG).show();
            }
        }
        return dal;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_main);
        purchaseButton = (Button) findViewById(R.id.button);
        amount = (EditText) findViewById(R.id.editText);
        callTrx = (EditText) findViewById(R.id.callTrx);
        tidSelection = (EditText) findViewById(R.id.tidSelection);
        result = (TextView) findViewById(R.id.result);
        preauthButton = (Button) findViewById(R.id.buttonPreauth);
        topUpButton = (Button) findViewById(R.id.topUpButton);
        preauthCompletionButton = (Button) findViewById(R.id.preauthCompletionButton);
        amountPreauth = (EditText) findViewById(R.id.amountPreauth);
        reversalButton = (Button) findViewById(R.id.buttonReversal);
        refundButton = (Button) findViewById(R.id.buttonRefund);
        amountConfirmation = (CheckBox) findViewById(R.id.amountConfirmation);
        printPedReceipt = (CheckBox) findViewById(R.id.printPedReceipt);
        onlineTotalsButton = (Button) findViewById(R.id.buttonOnlineTotals);
        terminalstatus = (TextView) findViewById(R.id.terminalstatus);
        closeSessionButton = (Button) findViewById(R.id.buttonCloseSession);
        receiptView = (TextView) findViewById(R.id.receiptTextView);
        selectTID = (Button) findViewById(R.id.SelectTID);
        scannerButton = (Button) findViewById(R.id.scannerButton);
        extractFont("fonts/RobotoMono-Regular.ttf");
        scannerButton2 = (Button) findViewById(R.id.scannerButton2);
        pref = getApplicationContext().getSharedPreferences("MyPref", 0); // 0 - for private mode
        SharedPreferences.Editor editor = pref.edit();
        int callTrzId = pref.getInt("callTrzId", -1);
        final AlertDialog alertDialog = new AlertDialog.Builder(MainActivity.this).create();
        alertDialog.setTitle("Alert");
        alertDialog.setMessage("ENTER AMOUNT");




        switch (checkAppStart()) {
            case NORMAL:
                callTrzId++;
                editor.putInt("callTrzId", callTrzId);
                editor.commit();
                callTrzId = pref.getInt("callTrzId", -1);
                break;
            case FIRST_TIME_VERSION:

                callTrzId = 0;
                editor.putInt("callTrzId", 0);
                editor.commit();

                break;
            case FIRST_TIME:

                callTrzId = 0;
                editor.putInt("callTrzId", 0);
                editor.commit();

                break;
            default:
                break;
        }


        if (getIntent() != null
                && getIntent().getData() != null
                && getIntent().getData().getHost() != null
                && (getIntent().getData().getHost().equals(PAYMENT_RESULT_HOST) ||
                (getIntent().getData().getHost().equals(SERVICE_RESULT_HOST)))) {

            Uri data = getIntent().getData();
            String result = data.getQueryParameter(Parameters.RESULT);
            String message = data.getQueryParameter(Parameters.RESULT_MESSAGE);
            String optMessage = data.getQueryParameter(Parameters.RESULT_OPT_MESSAGE);
            String field47 = stringConversionToTagDF8105(data.getQueryParameter(Parameters.CALLER_TRX_ID).toCharArray()).toString();
            String field47ASCII = data.getQueryParameter(Parameters.CALLER_TRX_ID);

            Context receiptView = MainActivity.receiptView.getContext();

            if (getIntent().getData().getHost().equals(PAYMENT_RESULT_HOST)) {

                Intent intent = new Intent(MainActivity.result.getContext(), PageComposingActivity.class);
                intent.putExtra("receiptView", String.valueOf(receiptView));
                startActivity(intent);
            }
            Uri extendedResultUri = null;
            if (getIntent().getExtras() != null) {
                extendedResultUri = getIntent().getExtras().getParcelable(Parameters.EXTENDED_RESULT_DATA_URI);
            }

            if (extendedResultUri != null) {
                parseExtendedResult(extendedResultUri);

            }
            if (!TextUtils.isEmpty(result)) {
                this.result.setVisibility(View.VISIBLE);
                this.result.append("Result: " + result);
                this.result.append(System.getProperty("line.separator"));
                this.result.append("Mess:" + message);
                this.result.append(System.getProperty("line.separator"));
                this.result.append("Otional message: " + optMessage);
                this.result.append(System.getProperty("line.separator"));
                this.result.append("Caller trx ID: " + callTrzId);
                this.result.append(System.getProperty("line.separator"));
                this.result.append("Field 47 in HEX: " + field47);
                this.result.append(System.getProperty("line.separator"));
                this.result.append("Field 47 in ASCII: " + field47ASCII);
            }
            finish();
        }


        appContext = getApplicationContext();
        dal = getDal();



        purchaseButton.setOnClickListener(new View.OnClickListener() {


            @Override
            public void onClick(View view) {

                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(PAYMENT_RESULT_HOST);

                String uriResponse = builder.build().toString();

                MainActivity mActivity = new MainActivity();
                StringBuilder TagDF8105 = mActivity.stringConversionToTagDF8105(callTrx.getText().toString().toCharArray());
                if( amount.getText().toString().isEmpty()){
                    alertDialog.show();
                }
                else {
                    builder.scheme("pax")
                            .authority("payment")
                            .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                            .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.PAYMENT.toString())
                            .appendQueryParameter(Parameters.AMOUNT, amount.getText().toString())
                            .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                            .appendQueryParameter(Parameters.AMOUNT_CONFIRMATION, Boolean.toString(amountConfirmation.isChecked()))
                            .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse)
                            .appendQueryParameter(Parameters.RECEIPT_BY_ECR, Boolean.toString(printPedReceipt.isChecked()))
                            .appendQueryParameter(Parameters.ADDITIONAL_FIELD_47_DATA, "" + TagDF8105);


                    Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });

        refundButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(PAYMENT_RESULT_HOST);

                String uriResponse = builder.build().toString();

                MainActivity mActivity = new MainActivity();
                StringBuilder TagDF8105 = mActivity.stringConversionToTagDF8105(callTrx.getText().toString().toCharArray());
                if( amount.getText().toString().isEmpty()){
                    alertDialog.show();
                }
                else {
                    builder = new Uri.Builder();
                    builder.scheme("pax")
                            .authority("payment")
                            .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                            .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.REFUND.toString())
                            .appendQueryParameter(Parameters.AMOUNT, amount.getText().toString())
                            .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                            .appendQueryParameter(Parameters.AMOUNT_CONFIRMATION, Boolean.toString(amountConfirmation.isChecked()))
                            .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse)
                            .appendQueryParameter(Parameters.RECEIPT_BY_ECR, Boolean.toString(printPedReceipt.isChecked()))
                            .appendQueryParameter(Parameters.ADDITIONAL_FIELD_47_DATA, "" + TagDF8105);

                    Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });


        reversalButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(PAYMENT_RESULT_HOST);

                String uriResponse = builder.build().toString();

                MainActivity mActivity = new MainActivity();
                StringBuilder TagDF8105 = mActivity.stringConversionToTagDF8105(callTrx.getText().toString().toCharArray());
                if( amount.getText().toString().isEmpty()){
                    alertDialog.show();
                }
                else {
                    builder = new Uri.Builder();
                    builder.scheme("pax")
                            .authority("payment")
                            .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                            .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.REVERSAL.toString())
                            .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                            .appendQueryParameter(Parameters.AMOUNT_CONFIRMATION, Boolean.toString(amountConfirmation.isChecked()))
                            .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse)
                            .appendQueryParameter(Parameters.RECEIPT_BY_ECR, Boolean.toString(printPedReceipt.isChecked()))
                            .appendQueryParameter(Parameters.ADDITIONAL_FIELD_47_DATA, "" + TagDF8105);

                    Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });


        preauthButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(PAYMENT_RESULT_HOST);

                String uriResponse = builder.build().toString();
                if( amount.getText().toString().isEmpty()){
                    alertDialog.show();
                }
                else {
                    builder = new Uri.Builder();
                    builder.scheme("pax")
                            .authority("payment")
                            .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                            .appendQueryParameter(Parameters.AMOUNT, amount.getText().toString())
                            .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.PREAUTHORIZATION.toString())
                            .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                            .appendQueryParameter(Parameters.AMOUNT_CONFIRMATION, Boolean.toString(amountConfirmation.isChecked()))
                            .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse);

                    Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                    startActivity(intent);
                }
            }
        });


        topUpButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, PreauthTopUp.class);
                startActivity(intent);
            }
        });


        preauthCompletionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, Preauth.class);
                startActivity(intent);
            }
        });

        selectTID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent intent = new Intent(MainActivity.this, SelectTID.class);
                startActivity(intent);
            }
        });

        closeSessionButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(SERVICE_RESULT_HOST);

                String uriResponse = builder.build().toString();

                builder = new Uri.Builder();
                builder.scheme("pax")
                        .authority("service")
                        .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                        .appendQueryParameter(Parameters.SERVICE_OPERATION, ServiceOperation.CLOSE_SESSION.toString())
                        .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                        .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse);

                Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });


        onlineTotalsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Uri.Builder builder = new Uri.Builder();
                builder.scheme("demopax")
                        .authority(SERVICE_RESULT_HOST);

                String uriResponse = builder.build().toString();

                builder = new Uri.Builder();
                builder.scheme("pax")
                        .authority("service")
                        .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                        .appendQueryParameter(Parameters.SERVICE_OPERATION, ServiceOperation.ONLINE_TOTALS.toString())
                        .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                        .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse);

                Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);
                startActivity(intent);
            }
        });

        scannerButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainActivity.this, DALTestActivity.class);
                startActivity(intent);
            }
        });

        scannerButton2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                handler = new Handler();

                GetObj ok = new GetObj(MainActivity.this);
                ok.ID();

                ScannerTester sc = new ScannerTester(REAR);
                ScannerTester o = sc.getInstance(REAR);

                sc.scan(handler, 100000);
                displayScanResult(handler.obtainMessage().toString());

            }
        });

    }
    /*
     * When the Activity of the app that hosts files sets a result and calls
     * finish(), this method is invoked. The returned Intent contains the
     * content URI of a selected file. The result code indicates if the
     * selection worked or not.
     */

    @Override
    public void onActivityResult(int requestCode, int resultCode,
                                 Intent returnIntent) {

        do {

            if (resultCode ==  -1) {

                IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, returnIntent);
                if (result != null) {
                    if (result.getContents() == null) {

                        Toast.makeText(this, "Cancelled", Toast.LENGTH_LONG).show();

                    } else {

                        Toast.makeText(this, "Scanned: " + result.getContents(), Toast.LENGTH_LONG).show();
                    }
                } else {
                    super.onActivityResult(requestCode, resultCode, returnIntent);
                }

            }
            if (resultCode != RESULT_OK) {
                    break;
            }

            // Get the file's content URI from the incoming Intent
            Uri returnUri = returnIntent.getData();
            if (returnUri == null) {
                break;
            }

            /*
             * Try to open the file for "read" access using the
             * returned URI. If the file isn't found, write to the
             * error log and return.
             */
            ParcelFileDescriptor pfd = null;
            try {
                /*
                 * Get the content resolver instance for this context, and use it
                 * to get a ParcelFileDescriptor for the file.
                 */
                pfd = getContentResolver().openFileDescriptor(returnUri, "r");
            } catch (FileNotFoundException e) {
                Log.e("app2app", "File not found.");
                break;
            }

            // Get a regular file descriptor for the file
            FileDescriptor fd = pfd.getFileDescriptor();
            if (fd == null) {
                try {
                    pfd.close();
                } catch (IOException e) {
                    Log.e("A2A", e.getMessage());
                }
                break;
            }

            // Read file content
            try (FileInputStream fileInputStream = new FileInputStream(fd)) {
                long size = fileInputStream.available();

                if (size <= 0) {
                    break;
                }

                byte[] data = new byte[(int) size];
                fileInputStream.read(data);

                String jsonData = new String(data);
                JSONObject jObject = new JSONObject(jsonData);
                if (jObject == null) {
                    break;
                }

                String model = jObject.getString(Configuration.MODEL);
                Log.d("A2A", "Terminal model: " + model);

                String serialno = jObject.getString(Configuration.SERIAL_NO);
                Log.d("A2A", "Serial number: " + serialno);

                String packageVersion = jObject.getString(Configuration.PACKAGE_VERSION);
                Log.d("A2A", "Package version: " + packageVersion);

                String cb2Version = jObject.getString(Configuration.CB2_VERSION);
                Log.d("A2A", "CB2 version: " + cb2Version);

                String imei = jObject.getString(Configuration.IMEI);
                Log.d("A2A", "Terminal IMEI: " + imei);

                JSONArray tidList = jObject.getJSONArray(Configuration.TERMINAL_ID_LIST);

                terminalstatus.append("DHK: " + tidList + '\n');

                for (int p = 0; p < tidList.length(); p++) {
                    String r = tidList.getString(p);
                    receiptView.append("terminal list: " + r + '\n');
                }


                if (tidList == null) {
                    break;
                }

                if ((tidList != null) && (tidList.length() > 0)) {


                    for (int i = 0; i < tidList.length(); i++) {
                        JSONObject tidInfo = tidList.getJSONObject(i);
                        if (tidInfo != null) {
                            String tid = tidInfo.getString(Configuration.TERMINAL_ID);
                            String status = tidInfo.getString(Configuration.STATUS);

                            boolean onlineOpDone = tidInfo.getBoolean(Configuration.ONLINE_OPERATION_AFTER_FIRST_DLL);

                            receiptView.append("Terminal ID:" + tid + '\n');
                            receiptView.append("Terminal status:" + status + '\n');
                            receiptView.append("ONLINE OPERATION AFTER FIRST DLL : " + onlineOpDone + '\n');


                            if (tid != null) {
                                Log.d("A2A", "TID: " + tid + ", status: " + status + ", onlineOperationAfterFirstDll: " + onlineOpDone);

                            }
                        }
                    }
                }
            } catch (IOException e) {
                Log.e("A2A", e.getMessage());
            } catch (JSONException e) {
                Log.e("A2A", e.getMessage());
            }

        } while (false);
    }


    private void parseExtendedResult(Uri fileUri) {


        if (fileUri == null) {
            return;
        }

        do {
            /*          * Try to open the file for "read" access using the          * received URI.          */
            byte[] data = null;

            try (ParcelFileDescriptor pfd = getContentResolver().openFileDescriptor(fileUri, "r");
                 FileInputStream fileInputStream = new FileInputStream(pfd.getFileDescriptor())) {

                long size = fileInputStream.available();
                if (size <= 0) {
                    break;
                }

                data = new byte[(int) size];
                fileInputStream.read(data);

            } catch (FileNotFoundException e) {
                Log.e("app2app", "File not found: " + e.getMessage());
            } catch (IOException e) {
                Log.e("app2app", "IOException: " + e.getMessage());
            } catch (SecurityException e) {
                Log.e("app2app", "SecurityException: " + e.getMessage());
            }

            if (data == null) {
                break;
            }

            try {
                String jsonData = new String(data);
                JSONObject jObject = new JSONObject(jsonData);
                if (jObject == null) {
                    break;
                }

                String receipt = jObject.getString("receipt");

                if (receipt != null) {

                    purchaseButton.setVisibility(View.GONE);
                    preauthButton.setVisibility(View.GONE);
                    refundButton.setVisibility(View.GONE);
                    reversalButton.setVisibility(View.GONE);
                    closeSessionButton.setVisibility(View.GONE);
                    onlineTotalsButton.setVisibility(View.GONE);
                    amount.setVisibility(View.GONE);
                    callTrx.setVisibility(View.GONE);
                    scannerButton.setVisibility(View.GONE);
                    scannerButton2.setVisibility(View.GONE);

                    try {

                        if (receipt != null) {
                            receiptView.append('\n' + receipt);
                        } else {
                            receiptView.append('\n' + "receipt is null");

                        }

                    } catch (Exception e) {
                        receiptView.append('\n' + "stan-app2appData-NotReceived");
                    }


                    try {
                        String stan = null;
                        if (stan == null) {
                            stan = jObject.getString("stan");
                            receiptView.append('\n' + "stan: " + stan);
                        } else {
                            receiptView.append('\n' + "Stan is null");

                        }

                    } catch (Exception e) {
                        receiptView.append('\n' + "stan-app2appData-NotReceived");
                    }

                    try {
                        String onlineID = null;
                        if (onlineID == null) {
                            onlineID = jObject.getString("onlineID ");
                            receiptView.append('\n' + "onlineID: " + onlineID);
                        } else {
                            receiptView.append('\n' + "onlineID is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "onlineID-app2appData-NotReceived");
                    }

                    try {
                        String actionCode = null;
                        if (actionCode == null) {
                            actionCode = jObject.getString("actionCode ");
                            receiptView.append('\n' + "actionCode: " + actionCode);
                        } else {
                            receiptView.append('\n' + "actionCode is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "actionCode-app2appData-NotReceived");
                    }

                    try {
                        String maskedPan = null;
                        if (maskedPan == null) {
                            maskedPan = jObject.getString("maskedPan");
                            receiptView.append('\n' + "maskedPan: " + maskedPan);
                        } else {
                            receiptView.append('\n' + "maskedPan is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "maskedPan-app2appData-NotReceived");
                    }

                    try {
                        String cardInputType = null;
                        if (cardInputType == null) {
                            cardInputType = jObject.getString("cardInputType");
                            receiptView.append('\n' + "cardInputType: " + cardInputType);
                        } else {
                            receiptView.append('\n' + "cardInputType is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "cardInputType-app2appData-NotReceived\"");
                    }

                    try {
                        String cardType = null;
                        if (cardType == null) {
                            cardType = jObject.getString("cardType");
                            receiptView.append('\n' + "cardType: " + cardType);
                        } else {
                            receiptView.append('\n' + "cardType is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "cardType-app2appData-NotReceived");
                    }

                    try {
                        String authorizationCode = null;
                        if (authorizationCode == null) {
                            authorizationCode = jObject.getString("authorizationCode");
                            receiptView.append('\n' + "authorizationCode: " + authorizationCode);
                        } else {
                            receiptView.append('\n' + "authorizationCode is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "authorizationCode-app2appData-NotReceived");
                    }

                    try {
                        String hostTimeStamp = null;
                        if (hostTimeStamp == null) {
                            hostTimeStamp = jObject.getString("hostTimeStamp");
                            receiptView.append('\n' + "hostTimeStamp: " + hostTimeStamp);
                        } else {
                            receiptView.append('\n' + "hostTimeStamp is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "hostTimeStamp-app2appData-NotReceived");
                    }

                    try {
                        String acquirerID = null;
                        if (acquirerID == null) {
                            acquirerID = jObject.getString("acquirerID");
                            receiptView.append('\n' + "acquirerID: " + acquirerID);
                        } else {
                            receiptView.append('\n' + "acquirerID is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "acquirerID-app2appData-NotReceived");
                    }

                    try {
                        String acquirerName = null;
                        if (acquirerName == null) {
                            acquirerName = jObject.getString("acquirerName");
                            receiptView.append('\n' + "acquirerName: " + acquirerName);
                        } else {
                            receiptView.append('\n' + "acquirerName is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "acquirerName-app2appData-NotReceived");
                    }

                    try {
                        String applicationID = null;
                        if (applicationID == null) {
                            applicationID = jObject.getString("applicationID");
                            receiptView.append('\n' + "applicationID: " + applicationID);
                        } else {
                            receiptView.append('\n' + "applicationID is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "applicationID-app2appData-NotReceived");
                    }

                    try {
                        String applicationLabel = null;
                        if (applicationLabel == null) {
                            applicationLabel = jObject.getString("applicationLabel");
                            receiptView.append('\n' + "applicationLabel: " + applicationLabel);
                        } else {
                            receiptView.append('\n' + "applicationLabel is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "applicationLabel-app2appData-NotReceived");
                    }

                    try {
                        Boolean signatureRequired = null;
                        if (signatureRequired == null) {
                            signatureRequired = jObject.getBoolean("signatureRequired");

                            if (signatureRequired) {
                                receiptView.append('\n' + "Signature required");
                            }
                            if (!signatureRequired) {
                                receiptView.append('\n' + "Signature not required");
                            }

                        } else {
                            receiptView.append('\n' + "No signature required");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "field 62 Data-app2appData-NotReceived");
                    }

                    try {
                        String fiedl62Data = null;
                        if (fiedl62Data == null) {
                            fiedl62Data = jObject.getString("field62Data");
                            receiptView.append('\n' + "field 62: " + fiedl62Data);
                        } else {
                            receiptView.append('\n' + "field 62 is null");
                        }
                    } catch (Exception e) {
                        receiptView.append('\n' + "field 62 Data-app2appData-NotReceived");
                    }


                }
            } catch (JSONException e) {
                Log.e("app2app", "JSONException: " + e.getMessage());

            }

        } while (false);
    }


}

