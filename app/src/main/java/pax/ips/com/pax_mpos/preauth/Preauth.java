package pax.ips.com.pax_mpos.preauth;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import pax.ips.com.pax_mpos.App2AppData.Parameters;
import pax.ips.com.pax_mpos.App2AppData.enums.PaymentType;
import pax.ips.com.pax_mpos.BuildConfig;
import pax.ips.com.pax_mpos.MainActivity;
import pax.ips.com.pax_mpos.R;


public class Preauth extends MainActivity {


    private static final String PAYMENT_RESULT_HOST = "paymentresult";


    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.preauth);

        PreauthCode = (EditText) findViewById(R.id.PreauthCode);
        amountPreauth = (EditText) findViewById(R.id.amountPreauth);

        Button enter = (Button) findViewById(R.id.enter);
        enter.setOnClickListener(new View.OnClickListener() {

        @Override
        public void onClick(View arg0) {

            Uri.Builder builder = new Uri.Builder();
            builder.scheme("demopax")
                    .authority(PAYMENT_RESULT_HOST);

            String uriResponse = builder.build().toString();


            builder = new Uri.Builder();
            builder.scheme("pax")
                    .authority("payment")
                    .appendQueryParameter(Parameters.CALLER_PACKAGE, BuildConfig.APPLICATION_ID)
                    .appendQueryParameter(Parameters.AMOUNT, amountPreauth.getText().toString())
                    .appendQueryParameter(Parameters.PAYMENT_TYPE, PaymentType.CLOSE_PREAUTHORIZATION.toString())
                    .appendQueryParameter(Parameters.CALLER_TRX_ID, callTrx.getText().toString())
                    .appendQueryParameter(Parameters.AMOUNT_CONFIRMATION, Boolean.toString(amountConfirmation.isChecked()))
                    .appendQueryParameter(Parameters.RESPONSE_URI, uriResponse)
                    .appendQueryParameter(Parameters.PREAUTHORIZATION_CODE, PreauthCode.getText().toString());

            Intent intent = new Intent(Intent.ACTION_VIEW, builder.build());
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            startActivity(intent);
            finish();

            }


    });


}
}
