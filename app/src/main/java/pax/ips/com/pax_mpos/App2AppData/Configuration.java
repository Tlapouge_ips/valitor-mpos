package pax.ips.com.pax_mpos.App2AppData;

public class Configuration {

        public static final String MODEL = "model";
        public static final String SERIAL_NO = "serialNo";
        public static final String PACKAGE_VERSION = "packageVersion";
        public static final String CB2_VERSION = "cb2Version";
        public static final String IMEI = "imei";
        public static final String simSerialNumber = "simSerialNumber";
        public static final String TERMINAL_ID_LIST = "terminalIdList";
        public static final String index = "index";
        public static final String TERMINAL_ID = "terminalID";
        public static final String alias = "alias";
        public static final String STATUS = "status";
        public static final String ONLINE_OPERATION_AFTER_FIRST_DLL = "onlineOperationAfterFirstDll";
    }

